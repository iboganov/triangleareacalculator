﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriangleAreaCalculator;

namespace TriangleAreaCalculatorTests.RightTriangleTests
{
    [TestClass]
    public class CalculateAreaTests
    {
        [TestMethod]
        public void CalculateAreaTest()
        {
            var sideA = 3;
            var sideB = 4;
            var sideC = 5;

            var triangle = new RightTriangle(sideA, sideB, sideC);

            var area = triangle.CalculateArea();

            Assert.AreEqual(6, area);
        }
    }
}
