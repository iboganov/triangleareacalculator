﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TriangleAreaCalculator.Exceptions;
using TriangleAreaCalculator;

namespace TriangleAreaCalculatorTests.RightTriangleTests
{
    [TestClass]
    public class ConstructorTests
    {
        [TestMethod]
        [ExpectedException(typeof(NotRightTriangleException))]
        public void IfTriangleIsNotRight_ThrowNotRightTriangleException()
        {
            var rightTriangle = new RightTriangle(5, 3, 7);
        }
    }
}
