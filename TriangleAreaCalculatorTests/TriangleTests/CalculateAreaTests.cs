﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriangleAreaCalculator;

namespace TriangleAreaCalculatorTests.TriangleTests
{
    [TestClass]
    public class CalculateAreaTests
    {
        [TestMethod]
        public void CalculateAreaTest()
        {
            var sideA = 5;
            var sideB = 4;
            var sideC = 7;

            var triangle = new Triangle(sideA, sideB, sideC);

            var area = triangle.CalculateArea();

            Assert.AreEqual(9.7979589711327115, area);
        }
    }
}
