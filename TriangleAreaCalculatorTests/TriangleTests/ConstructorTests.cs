﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TriangleAreaCalculator;
using TriangleAreaCalculator.Exceptions;

namespace TriangleAreaCalculatorTests
{
    [TestClass]
    public class ConstructorTests
    {
        [TestMethod]
        [ExpectedException(typeof(InvalidSideLengthException))]
        public void IfAnyOfInputParametersAreEqualOrLessThanZero_ThrowInvalidSideLenthException()
        {
            var sideA = -1;
            var sideB = 0;
            var sideC = 1;

            var triangle = new Triangle(sideA, sideB, sideC);
        }


        [TestMethod]
        [ExpectedException(typeof(TriangleInequalityException))]
        public void IfSumOfTwoInputParametersEqualsOrLessThanLeftoverParameter_ThrowArgumentOutOfRangeException()
        {
            var sideA = 1;
            var sideB = 10000;
            var sideC = 2;

            var triangle = new Triangle(sideA, sideB, sideC);
        }
    }
}
