﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriangleAreaCalculator.Exceptions;

namespace TriangleAreaCalculator
{
    public class Triangle
    {
        protected List<double> _sides;
        protected virtual List<double> Sides
        {
            get
            {
                return _sides;
            }

            set
            {
                if (value.Any(side => side.IsZeroOrNegative()))
                    throw new InvalidSideLengthException("Side value cannot equals zero or be negaive");

                _sides = value;
            }
        }

        private double SideA
        {
            get
            {
                return Sides[0];
            }
        }


        private double SideB
        {
            get
            {
                return Sides[1];
            }
        }


        private double SideC
        {
            get
            {
                return Sides[2];
            }
        }


        public Triangle(double sideA, double sideB, double sideC)
        {
            Sides = new List<double> { sideA, sideB, sideC };

            CheckForInequality();
        }


        public virtual double CalculateArea()
        {
            var semiperimeter = (SideA + SideB + SideC) / 2;

            var result = Math.Sqrt(semiperimeter * (semiperimeter - SideA) * (semiperimeter - SideB) * (semiperimeter - SideC));

            return result;
        }


        private void CheckForInequality()
        {
            if (!((SideA + SideB > SideC) && (SideA + SideC > SideB) && (SideB + SideC > SideA)))
                throw new TriangleInequalityException();
        }
    }
}
