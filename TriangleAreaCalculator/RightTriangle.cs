﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriangleAreaCalculator.Exceptions;

namespace TriangleAreaCalculator
{
    public class RightTriangle : Triangle
    {
        protected override List<double> Sides
        {
            get
            {
                return _sides;
            }

            set
            {
                base.Sides = value;

                _sides.Sort();
            }
        }


        private double CathetusA
        {
            get
            {
                return Sides.First();
            }
        }


        private double CathetusB
        {
            get
            {
                return Sides.First(i => i != Hypotenuse && i != CathetusA);
            }
        }


        private double Hypotenuse
        {
            get
            { 
                return Sides.Last();
            }
        }


        public RightTriangle(double sideA, double sideB, double sideC) 
            : base(sideA, sideB, sideC)
        {
            CheckForRightTriangle();
        }


        private void CheckForRightTriangle()
        {
            if (Math.Pow(CathetusA, 2) + Math.Pow(CathetusB, 2) != Math.Pow(Hypotenuse, 2))
                throw new NotRightTriangleException();
        }


        public override double CalculateArea()
        {
            return (CathetusA * CathetusB) / 2;
        }
    }
}
