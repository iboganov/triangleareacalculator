﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TriangleAreaCalculator.Exceptions
{
    [Serializable]
    public class NotRightTriangleException : Exception
    {
        public NotRightTriangleException()
        {
        }


        public NotRightTriangleException(string message)
            : base(message)
        {
        }

        public NotRightTriangleException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected NotRightTriangleException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
