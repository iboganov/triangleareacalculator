﻿using System;
using System.Runtime.Serialization;

namespace TriangleAreaCalculator.Exceptions
{
    [Serializable]
    public class InvalidSideLengthException : ArgumentOutOfRangeException
    {
        public InvalidSideLengthException()
        {
        }


        public InvalidSideLengthException(string message, string paramName)
            : base(message, paramName)
        {
        }


        public InvalidSideLengthException(string message)
            : base(message)
        {
        }

        public InvalidSideLengthException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected InvalidSideLengthException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}