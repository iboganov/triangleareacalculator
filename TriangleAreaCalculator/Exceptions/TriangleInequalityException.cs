﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace TriangleAreaCalculator.Exceptions
{
    [Serializable]
    public class TriangleInequalityException : Exception
    {
        public TriangleInequalityException()
        {
        }


        public TriangleInequalityException(string message)
            : base(message)
        {
        }

        public TriangleInequalityException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected TriangleInequalityException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
