﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriangleAreaCalculator
{
    public static class DoubleExtensions
    {
        public static bool IsZeroOrNegative(this double value)
        {
            if (value <= 0)
                return true;

            return false;
        }
    }
}
